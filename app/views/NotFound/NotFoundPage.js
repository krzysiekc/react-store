import React from 'react';

export default class NotFoundPage extends React.Component {
  render() {
    return (
      <div>Page not found - 404</div>
    );
  }
}
