import React from "react";
import Catalog from "app/components/catalog/Catalog";

export default class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Catalog />
      </div>
    );
  }
}
;
