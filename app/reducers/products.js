import ProductRepository from "app/api/repository/ProductRepository";
import { SET_SORTING } from "app/actions/types";

let initialState = {
  list: ProductRepository.getAll(),
  sorting: {
    field: 'name',
    dir: 'asc'
  }
};

const products = (state = initialState, action) => {
  if (action.type === SET_SORTING) {
    const sorted = _.sortBy(state.list, [action.payload.field]);

    return {
      ...state,
      list: action.payload.dir === 'asc' ? sorted : sorted.reverse(),
      sorting: {
        field: action.payload.field,
        dir: action.payload.dir,
      }
    };
  }

  return state;
};

export default products;
