import React from "react";
import {render} from "react-dom";
import HomePage from "app/views/Home/HomePage";
import {Provider} from "react-redux";
import {combineReducers, createStore} from "redux";
import products from "app/reducers/products";

const store = createStore(combineReducers({products}));

render(
  <Provider store={store}>
    <HomePage/>
  </Provider>,
  document.querySelector('#app')
);
