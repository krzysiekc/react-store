import { SET_SORTING } from "app/actions/types";

export const setSorting = (field, dir) => {
  return {
    type: SET_SORTING,
    payload: {
      field,
      dir
    }
  }
};
