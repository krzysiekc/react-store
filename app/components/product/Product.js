import React from "react";

export default class Product extends React.Component {
  render() {
    const product = this.props.product;
    return (
      <div className="product">
        <img src={product.image}/>
        <span>{product.name}</span>
        <span>Cena: {product.price}</span>
        <span>Ilość: {product.quantity}</span>
      </div>
    );
  }
}
;
