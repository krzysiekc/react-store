import React from "react";
import Product from "app/components/product/Product";
import * as _ from "lodash";
import {connect} from "react-redux";
import SearchBar from "app/components/search/SearchBar";

class Catalog extends React.Component {

  render() {
    return (
      <div>
        <SearchBar/>
        {
          _.map(this.props.productList, (product) => (
            <Product
              key={product.id}
              product={product}/>
          ))
        }
      </div>
    );
  }
}
;

const mapStateToProps = (state) => {
  return {
    productList: state.products.list
  };
};
const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
