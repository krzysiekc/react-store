import * as React from "react";
import {setSorting} from "app/actions/actions";
import {connect} from "react-redux";

class SearchBar extends React.Component {
  handleChange = (e) => {
    this.props.setSorting(e.target.name, e.target.value)
  };

  render() {
    return (
      <div className="search-bar">
        <div className="search-bar__name-filter">
          <select onChange={this.handleChange} name="name">
            <option value="asc">Nazwa Ros</option>
            <option value="desc">Nazwa Mal</option>
          </select>
        </div>
        <div className="search-bar__price-filter">
          <select onChange={this.handleChange} name="price">
            <option value="asc">Cena Ros</option>
            <option value="desc">Cena Mal</option>
          </select>
        </div>
        <div className="search-bar__quantity-filter">
          <select onChange={this.handleChange} name="quantity">
            <option value="asc">Ilość Ros</option>
            <option value="desc">Ilość Mal</option>
          </select>
        </div>
      </div>
    );
  }
}
;

export default connect(
  state => ({}),
  { setSorting }
)(SearchBar);

