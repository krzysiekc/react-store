import Faker from "faker"

export default class ProductRepository {
  static getAll() {
    let products = [];
    for (let i = 0; i < 11; i++) {
      products.push({
        id: i + 1,
        image: 'http://lorempixel.com/100/100/',
        name: Faker.commerce.product(),
        price: Math.floor(Math.random() * 100),
        quantity: Math.floor(Math.random() * 11)
      });
    }

    return products;
  }
}
